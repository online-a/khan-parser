﻿namespace KhanParser.Processing {

    /// <summary> All information about the student. </summary>
    class KhanStudent
    {
        /// <summary> The UČO of a student. </summary>
        public int Uco { get; }

        /// <summary> The lectures completed by the student in Khan Academy. </summary>
        public int Lectures { get; }

        /// <summary> Is the UČO of the student valid? </summary>
        public bool IsValid { get; }

        /// <summary> All the fields in the export. </summary>
        public string[] Fields { get; }

        public KhanStudent(int uco, int lectures, string[] fields)
        {
            Uco = uco;
            IsValid = uco != -1;
            Lectures = lectures;
            Fields = fields;
        }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return Fields[0].Trim();
        }
    }
}