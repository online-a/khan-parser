﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace KhanParser.Processing
{
    static class KhanCSV
    {
        /// <summary> Parses a CSV file with Khan Academy export and returns the result. </summary>
        /// <param name="path"> A path to a valid CSV file. </param>
        /// <param name="nameColumn"> The number of the column containing name. </param>
        /// <param name="completionColumn"> The number of the column containing the date of last completion of the task. </param>
        /// <param name="dueDateColumn"> The number of the column containing the date of the task's due date. </param>
        /// <param name="delimiter"> A column separator of the CSV file. </param>
        /// <exception cref="ArgumentNullException"> Thrown when the file path is empty. </exception>
        /// <exception cref="ArgumentException"> Thrown when the file path doesn't exist, or doesn't lead to a CSV file. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the user entered incorrect column identifiers. </exception>
        public static IEnumerable<KhanRecord> ReadKhanCsv(string path, string nameColumn = "1", string completionColumn = "7", string dueDateColumn = "8", string delimiter = ",")
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException(nameof(path), "Je třeba vyplnit cestu k souboru.");
            }
            if (!System.IO.File.Exists(path) || !path.EndsWith(".csv", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new ArgumentException("Zadaná cesta buď neexistuje, nebo nevede k CSV souboru.", nameof(path));
            }

            var records = new List<KhanRecord>();
            using (var csvParser = new TextFieldParser(path))
            {
                csvParser.SetDelimiters(delimiter);
                csvParser.HasFieldsEnclosedInQuotes = true;

                //! The first row with column names
                int headersCount = csvParser.ReadFields().Length;

                if (!(int.TryParse(nameColumn, out int nameIndex) 
                   && int.TryParse(completionColumn, out int lastCompletedIndex)
                   && int.TryParse(dueDateColumn, out int dueDateIndex)))
                {
                    throw new InvalidOperationException("Některý ze zadaných pořadí sloupců (jméno nebo termíny) není platné číslo.");
                }
                if (!KhanHelper.AllBetween((1, headersCount), nameIndex, lastCompletedIndex, dueDateIndex))
                {
                    throw new IndexOutOfRangeException($"Některý ze zadaných pořadí sloupců (jméno nebo termíny) je mimo dostupný rozsah (od 1 do{headersCount}).");
                }

                nameIndex = Math.Max(0, nameIndex - 1);             // zero-based name column
                lastCompletedIndex = Math.Max(0, lastCompletedIndex - 1); // zero-based completion date column
                dueDateIndex = Math.Max(0, dueDateIndex - 1);               // zero-based due date column
                var ucoInName = new Regex(@"\d{4,}", RegexOptions.Compiled);

                while (!csvParser.EndOfData)
                {
                    // TODO: Yield
                    string[] record = csvParser.ReadFields();

                    // TODO: Filter when adding
                    KhanRecord parsedRecord = ParseRecord(record, (nameIndex, ucoInName), lastCompletedIndex, dueDateIndex);
                    records.Add(parsedRecord);
                }
            }

            static KhanRecord ParseRecord(string[] record, (int, Regex) ucoInfo, int lastCompletedIndex, int dueDateIndex)
            {
                (int nameIndex, Regex ucoInName) = ucoInfo;
                int uco = NameToUco(record[nameIndex], ucoInName); // name column


                DateTime? lastCompleted = KhanDate.GetDateTime(record[lastCompletedIndex].Trim()); // last completed date
                DateTime? due = KhanDate.GetDateTime(record[dueDateIndex].Trim()); // task due date

                return new KhanRecord(uco, lastCompleted, due, record);
            }

            return records;
        }

        /// <summary> Extracts UČO from the end of the student's name. Returns -1 if not found. </summary>
        /// <param name="name"> The name of the student in Khan Academy. </param>
        /// <param name="regex"> The regular expression to use to find people's UČO in their name. </param>
        static int NameToUco(string name, Regex regex)
        {
            name = name.Trim();
            if (!string.IsNullOrWhiteSpace(name))
            {
                Match nameMatch = regex.Match(name);
                if (nameMatch.Success && int.TryParse(nameMatch.Value, out int uco))
                {
                    return uco;
                }
            }
            return -1;
        }

    }
}
