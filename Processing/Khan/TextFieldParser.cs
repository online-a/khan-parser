using System;
using System.IO;
using System.Text.RegularExpressions;

namespace KhanParser.Processing
{
    public class TextFieldParser : StreamReader
    {
        char[] delimiters;
        string currentLine;
        Regex quotedFieldRegex;

        public TextFieldParser(string path) : base(path) { }

        public TextFieldParser(Stream stream) : base(stream) { }

        public string[] ReadFields()
        {
            currentLine = ReadLine();
            return GetFields();
        }

        public void SetDelimiters(string delim)
        {
            delimiters = delim.ToCharArray();
            const string evenQuotationMarks = "(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))";
            quotedFieldRegex = new Regex($"[{string.Join(string.Empty, delimiters)}]{evenQuotationMarks}", RegexOptions.Compiled);
        }

        /// <summary> Splits the line into fields – and removes quotations. </summary>
        /// <returns> An array of strings from the parsed CSV file.</returns>
        /// <exception cref="Exception"> Attempting to parse CSV file without defining any delimiters. </exception>
        string[] GetFields()
        {
            if (delimiters == null || delimiters.Length == 0)
            {
                throw new Exception($"{GetType().Name} requires delimiters be defined to identify fields.");
            }
            
            // Split by commas followed by even number of quotation marks
            string[] splitLine = !HasFieldsEnclosedInQuotes 
                    ? currentLine.Split(delimiters) 
                    : quotedFieldRegex.Split(currentLine); // "Nov 22, 16:59"
            if (HasFieldsEnclosedInQuotes)
            {
                for (int i = 0; i < splitLine.Length; i++)
                {
                    splitLine[i] = splitLine[i].Trim('"');
                }
            }
            return splitLine; // TODO: yield
        }

        public bool HasFieldsEnclosedInQuotes { get; set; } = false;

        public bool EndOfData
        {
            get { return EndOfStream; }
        }
    }
}
