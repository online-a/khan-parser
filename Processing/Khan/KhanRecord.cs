﻿using System;

namespace KhanParser.Processing {
    /// <summary> A single line from a Khan Academy export. </summary>
    class KhanRecord
    {
        /// <summary> The UČO of a student. </summary>
        public int Uco { get; }

        /// <summary> The name of a student as displayed in Khan Academy. </summary>
        public string Name => Fields[1].Trim();

        /// <summary> Displays the UČO of a student, or their name only if it's invalid. </summary>
        public string UcoOrName => IsValid ? Uco.ToString() : Name;

        /// <summary> Is the UČO of the student valid? </summary>
        public bool IsValid { get; }

        /// <summary> Checks if the record's due date falls between the two chosen dates. </summary>
        public bool IsRelevant(DateTime from, DateTime to)
        {
            return DueDate.IsBetween(from, to);
        }

        /// <summary> Checks if the record's due date falls between the two chosen dates. </summary>
        public bool WasCompleted(DateTime from, DateTime to)
        {
            return LastCompleted.IsBetween(from, to) // happened in the specific week
                && LastCompleted.Date <= DueDate; // probably always true
        }

        /// <summary> The last date of completion of a task in Khan Academy. </summary>
        public DateTime LastCompleted { get; }

        /// <summary> The due date of a task in Khan Academy. </summary>
        public DateTime DueDate { get; }

        /// <summary> All the fields in the export. </summary>
        public string[] Fields { get; }

        public KhanRecord(int uco, DateTime? lastCompleted, DateTime? due, string[] fields)
        {
            Uco = uco;
            IsValid = Uco != -1;
            LastCompleted = lastCompleted ?? DateTime.MaxValue;
            DueDate = due ?? DateTime.MinValue;
            Fields = fields;
        }

        /// <summary> Displays the name of a student. </summary>
        public override string ToString()
        {
            return Name;
        }
    }
}
