﻿using System;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.Linq;

namespace KhanParser.Processing
{
    public static class KhanDate
    {
        /// <summary> Czech locale. </summary>
        static readonly CultureInfo Cz = new CultureInfo("cs-CZ");

        /// <summary> English locale. </summary>
        static readonly CultureInfo En = new CultureInfo("en-US");

        /// <summary> Returns the date one day after today. </summary>
        public static DateTime Tomorrow
        {
            get { return DateTime.Today.AddDays(1); }
        }

        /// <summary> Returns the date of <see cref="DayOfWeek.Monday"/> in the previous week. </summary>
        public static DateTime LastMonday
        {
            get { return DateTime.Today.StartOfWeek(DayOfWeek.Monday).AddDays(-7); }
        }

        /// <summary> Returns the date of <see cref="DayOfWeek.Sunday"/> in the previous week. </summary>
        public static DateTime LastSunday
        {
            get { return LastMonday.AddDays(6); }
        }

        /// <summary> Returns the date one day after today the previous year. </summary>
        public static DateTime LastYear
        {
            get { return DateTime.Today.AddYears(-1).AddDays(1).Date; }
        }

        /// <summary> Returns a <see cref="DateTime"/> from the Khan Academy's timestamp. </summary>
        /// <param name="dateTimeString">The full column.</param>
        /// <param name="monthTolerance">If the due date didn't happen yet, shift the year - unless it results in more than ? month difference from today.</param>
        public static DateTime? GetDateTime(string dateTimeString, int monthTolerance = 6)
        {
            if (string.IsNullOrWhiteSpace(dateTimeString)) return null;

            string dt = dateTimeString = dateTimeString.Trim().Replace("st", "").Replace("nd", "").Replace("rd", "").Replace("th", "");
            if (!DateTime.TryParseExact(dt, "MMM d, h:mtt", En, DateTimeStyles.None, out DateTime date))
            {
                //! Not English → try Czech
                dt = dt.Replace("AM", " dop.").Replace("PM", " odp.");
                if (!DateTime.TryParseExact(dt, "MMM d., h:m tt", Cz, DateTimeStyles.None, out date))
                {
                    throw new ArgumentException($"Formát data nebyl rozpoznán: {dateTimeString}. Očekává se datum ve formátu ‘Sep 27., 12:13PM’ nebo ‘zář 27., 12:13PM’.");
                }
            }
            else if (DateTime.Today < date.Date) // Due date didn't happen yet
            {
                // OK, but wrong year
                DateTime lastYearDate = date.AddYears(-1);
                if (DateTime.Today < lastYearDate.AddMonths(monthTolerance)) // the timespan is not too big (which would suggest an error)
                {
                    date = lastYearDate;
                }
            }
            return date;
        }

        /// <summary> Checks if a date is between two other dates. </summary>
        /// <param name="date"> The date to check. </param>
        /// <param name="from"> The lower bound for the date. </param>
        /// <param name="to"> The upper bound for the date. </param>
        /// <returns> True if the date happened later than <paramref name="from"/> and earlier than <paramref name="to"/>.</returns>
        public static bool IsBetween(this DateTime date, DateTime from, DateTime to)
        {
            return (date.Date >= from.Date && date.Date <= to.Date);
        }

        /// <summary> Returns the <see cref="DateTime"/> of the start of the week. </summary>
        /// <param name="dt"></param>
        /// <param name="startOfWeek"></param>
        /// <returns></returns>
        static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        /// <summary> Returns the number of years between two dates. </summary>
        public static int YearsBetween(this DateTime from, DateTime to)
        {
            DateTime[] both = {from, to};
            var zeroTime = new DateTime(1, 1, 1);
            TimeSpan span = both.Max() - both.Min();
            return (zeroTime + span).Year - 1;
        }

        /// <summary> Gets <see langword="st"/>, <see langword="nd"/> and <see langword="rd"/> from a day number. </summary>
        /// <param name="integer"> The day of the month. </param>
        public static string ToOccurrenceSuffix(this int integer)
        {
            switch (integer % 100)
            {
                case 11:
                case 12:
                case 13:
                    return "th";
            }
            return (integer % 10) switch
            {
                1 => "st",
                2 => "nd",
                3 => "rd",
                _ => "th",
            };
        }
    }
}
