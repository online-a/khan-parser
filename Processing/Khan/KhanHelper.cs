﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KhanParser.Processing
{
    static class KhanHelper
    {
        /// <summary> Checks if a number is between two numbers. </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="lower">The (inclusive) lower bound for the <paramref name="value"/>.</param>
        /// <param name="upper">The (exclusive) upper bound for the <paramref name="value"/>.</param>
        /// <returns></returns>
        public static bool IsBetween(this int value, int lower, int upper)
        {
            return value >= lower && value < upper;
        }

        /// <summary> Checks if all numbers are <see cref="IsBetween">between</see> given bounds. </summary>
        /// <param name="bounds"> The (lower, upper) bounds of the comparison. </param>
        /// <param name="values"> The values to compare. </param>
        public static bool AllBetween((int Lower, int Upper) bounds, params int[] values)
        {
            (int lower, int upper) = bounds;
            return values.All(value => value.IsBetween(lower, upper));
        }
    }
}
