﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace KhanParser.Processing.IS
{
    static class MuniCSV
    {
        const string CourseCode = "Online_A";
        const char Delimiter = ':';
        const string Extension = "txt";

        /// <summary> Writes the Khan Academy export into a file. </summary>
        public static void WriteMuniCsv(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            var wa = (KhanHome.OutputData) doWorkEventArgs.Argument;

            if (string.IsNullOrWhiteSpace(wa.folderPath))
            {
                throw new ArgumentNullException(nameof(wa.folderPath), "Je třeba vyplnit cestu k výstupní složce.");
            }
            if (!Directory.Exists(wa.folderPath))
            {
                throw new ArgumentException("Zadaná cesta buď neexistuje, nebo nevede ke složce.", nameof(wa.folderPath));
            }
            
            var sb = new StringBuilder();
            var sbInvalid = new StringBuilder();
            
            // 481007/ONLINE_A/710073:*10
            List<KhanRecord> records = !wa.sorted 
                    ? wa.records 
                    : wa.records.OrderBy(rec => rec.Uco).ThenBy(rec => rec.Name).ToList();

            (DateTime fromDate, DateTime toDate) = wa.date;
            var groupedRecords = (from record in records
                                 group record by record.UcoOrName
                                 into recordGroup
                                 select new
                                 {
                                         UcoOrName = recordGroup.Key,
                                         IsValid = recordGroup.All(rec => rec.IsValid),
                                         Count = recordGroup.Count(rec => rec.WasCompleted(fromDate, toDate))
                                 }).ToList();

            for (int i = 0; i < groupedRecords.Count; i++)
            {
                var studentRecords = groupedRecords[i];
                int points = Math.Clamp(studentRecords.Count * wa.pointsPerLecture, 0, wa.pointsLimit);

                if (studentRecords.IsValid)
                {
                    (sender as BackgroundWorker)?.ReportProgress(Convert.ToInt32(Math.Clamp(i+1 * 100f / groupedRecords.Count, 0, 100)));
                    sb.AppendLine($"{studentRecords.UcoOrName}/{CourseCode}/{wa.ropot}{Delimiter}*{points}");
                }
                else
                {
                    sbInvalid.AppendLine($"*{points}\t\t{studentRecords.UcoOrName}");
                }
            }

            string outputFilePath = Path.Combine(wa.folderPath, $"ONLINE_A_Khan_Week_X.{Extension}");
            string invalidFilePath = Path.Combine(wa.folderPath, $"ONLINE_A_Khan_Invalid.{Extension}");
            File.WriteAllText(outputFilePath, sb.ToString()); 
            File.WriteAllText(invalidFilePath, sbInvalid.ToString()); 
            (sender as BackgroundWorker)?.ReportProgress(100);

            // Open folder
            Process.Start("explorer.exe", wa.folderPath);
        }
    }
}
