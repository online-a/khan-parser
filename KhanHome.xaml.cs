﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using KhanParser.Processing;
using KhanParser.Processing.IS;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace KhanParser
{
    /// <summary>
    /// Interaction logic for KhanHome.xaml
    /// </summary>
    public partial class KhanHome : Page
    {
        BackgroundWorker worker;

        //? Publishing single file
        //! dotnet publish -r win10-x64 -p:PublishSingleFile=true -c Release

        public KhanHome()
        {
            InitializeComponent();
            InitializeBackgroundWorker();
        }

        void InitializeBackgroundWorker()
        {
            worker = new BackgroundWorker {WorkerReportsProgress = true};
            worker.DoWork += MuniCSV.WriteMuniCsv;
            worker.ProgressChanged += Worker_ProgressChanged;
        }

        /// <summary> Validates if the text box contains a valid decimal number to convert to points. </summary>
        void PointValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var pointsRegex = new Regex(@"^[0-9]*\.?[0-9]+$");
            e.Handled = pointsRegex.IsMatch(e.Text);
        }

        /// <summary> Validates if the text box contains a whole number. </summary>
        void WholeNumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var wholeNumberRegex = new Regex(@"[^0-9]+");
            e.Handled = wholeNumberRegex.IsMatch(e.Text);
        }

        /// <summary> Validates that the input button is before today and the other day after. </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DatePicker_OnSelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime? fromDate = FromDatePicker?.SelectedDate;
            DateTime? toDate = ToDatePicker?.SelectedDate;

            if (fromDate.HasValue && toDate.HasValue)
            {
                if (toDate.Value.Date < fromDate.Value.Date)
                {
                    ToDatePicker.SelectedDate = fromDate.Value.Date.AddDays(1).AddSeconds(-1);
                }

            }

            e.Handled = true;
        }

        /// <summary> Find a csv file. </summary>
        void InputFileBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                    DefaultExt = ".csv",
                    Filter = "Khan Academy výstup (*.csv)|*.csv|Všechny soubory (*.*)|*.*",
                    FilterIndex = 0
            };

            // Display the dialog
            if (ofd.ShowDialog() == true)
            {
                InputFileTextBox.Text = ofd.FileName;
                if (string.IsNullOrWhiteSpace(OutputFolderTextBox.Text))
                {
                    OutputFolderTextBox.Tag = Path.GetDirectoryName(ofd.FileName) ?? string.Empty;
                }
            }
        }

        void OutputFolderBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            using var fbd = new FolderBrowserDialog
            {
                    ShowNewFolderButton = true,
                    UseDescriptionForTitle = true,
                    Description = "Výstupní složka"
            };

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                OutputFolderTextBox.Text = fbd.SelectedPath;
            }
        }

        /// <summary> Starting the conversion. </summary>
        void ConversionButton_Click(object sender, RoutedEventArgs e)
        {
            List<KhanRecord> khanRecords = KhanCSV.ReadKhanCsv(InputFileTextBox.Text,
                                                               NameColumnTextBox.Text,
                                                               CompletionDateColumnTextBox.Text,
                                                               DueDateColumnTextBox.Text).ToList();
            (DateTime from, DateTime to) = (FromDatePicker.SelectedDate.GetValueOrDefault(DateTime.MaxValue), 
                                            ToDatePicker.SelectedDate.GetValueOrDefault(DateTime.MinValue));

            // TODO: Why was it commented out?
            // ConversionProgressBar.Minimum = 0;
            // ConversionProgressBar.Maximum = khanRecords.Count;
            // ConversionProgressBar.Value = 0;

            if (string.IsNullOrWhiteSpace(RopotIdTextBox.Text))
            {
                throw new ArgumentNullException(nameof(RopotIdTextBox), "Je třeba vyplnit číselný identifikátor poznámkového bloku.");
            }

            string outputFolderPath = OutputFolderTextBox.Text.Trim();
            if (string.IsNullOrWhiteSpace(outputFolderPath))
            {
                string inputFolderPath = Path.GetDirectoryName(InputFileTextBox.Text.Trim()) ?? string.Empty;
                outputFolderPath = OutputFolderTextBox.Text = inputFolderPath;
            }
            
            var workerArgument = new OutputData(outputFolderPath,
                                                (from, to),
                                                khanRecords,
                                                RopotIdTextBox.Text,
                                                int.Parse(PointsPerLessonTextBox.Text.Trim()),
                                                int.Parse(PointLimitTextBox.Text.Trim()),
                                                SortedCheckbox.IsChecked.GetValueOrDefault(false));
            worker.RunWorkerAsync(workerArgument); // Start the conversion
        }

        internal struct OutputData
        {
            public string folderPath;
            public (DateTime from, DateTime to) date;
            public List<KhanRecord> records;
            public string ropot;
            public int pointsPerLecture;
            public int pointsLimit;
            public bool sorted;

            /// <summary>Initializes a new instance of the <see cref="OutputData" /> class.</summary>
            public OutputData(string folderPath, (DateTime from, DateTime to) date, List<KhanRecord> records, string ropot, int pointsPerLecture, int pointsLimit, bool sorted = true)
            {
                this.folderPath = folderPath;
                this.date = date;
                this.records = records;
                this.ropot = ropot;
                this.pointsPerLecture = pointsPerLecture;
                this.pointsLimit = pointsLimit;
                this.sorted = sorted;
            }
        }

        void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ConversionProgressBar.Value = e.ProgressPercentage;
        }
    }
}
